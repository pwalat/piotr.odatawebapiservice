﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.OData.Builder;
using Microsoft.Data.Edm;

namespace Piotr.ODataWebApiService.Service.Models
{
    public class ModelBuilder
    {
        public IEdmModel Build()
        {
            ODataModelBuilder modelBuilder = new ODataConventionModelBuilder();
            modelBuilder.EntitySet<Album>("Albums");
            modelBuilder.EntitySet<Artist>("Artists");
            modelBuilder.EntitySet<Genre>("Genres");

            return modelBuilder.GetEdmModel();
        }
    }
}
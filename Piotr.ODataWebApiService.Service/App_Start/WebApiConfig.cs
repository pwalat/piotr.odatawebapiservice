﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Microsoft.Data.Edm;
using Piotr.ODataWebApiService.Service.Models;

namespace Piotr.ODataWebApiService.Service
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var modelBuilder = new ModelBuilder();
            IEdmModel model = modelBuilder.Build();
            config.Routes.MapODataRoute("OData", null, model);
            config.EnableQuerySupport();
        }
    }
}

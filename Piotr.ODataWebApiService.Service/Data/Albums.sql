﻿CREATE TABLE [Album]
(
    [AlbumId] INT NOT NULL IDENTITY,
    [Title] NVARCHAR(160) NOT NULL,
    [ArtistId] INT NOT NULL,
	[GenreId] INT NOT NULL,
	[ReleaseDate] DATETIME,
    CONSTRAINT [PK_Album] PRIMARY KEY  ([AlbumId])
);

CREATE TABLE [Artist]
(
    [ArtistId] INT NOT NULL IDENTITY,
    [Name] NVARCHAR(120),
    CONSTRAINT [PK_Artist] PRIMARY KEY  ([ArtistId])
);

CREATE TABLE [Genre]
(
    [GenreId] INT NOT NULL IDENTITY,
    [Name] NVARCHAR(120),
    [Description] NVARCHAR(1020),
    CONSTRAINT [PK_Genre] PRIMARY KEY  ([GenreId])
);

ALTER TABLE [Album] ADD CONSTRAINT [FK_AlbumArtistId]
    FOREIGN KEY ([ArtistId]) REFERENCES [Artist] ([ArtistId]) ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE INDEX [IFK_AlbumArtistId] ON [Album] ([ArtistId]);

ALTER TABLE [Album] ADD CONSTRAINT [FK_AlbumGenreId]
    FOREIGN KEY ([GenreId]) REFERENCES [Genre] ([GenreId]) ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE INDEX [IFK_AlbumGenreId] ON [Album] ([GenreId]);
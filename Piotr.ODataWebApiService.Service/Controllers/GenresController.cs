﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using Microsoft.Data.OData;
using Microsoft.Data.OData.Query;
using Piotr.ODataWebApiService.Service.Models;

namespace Piotr.ODataWebApiService.Service.Controllers
{
    [ODataRouting]
    [ODataFormatting]
    public class GenresController : ApiController
    {
        private AlbumsContext _db = new AlbumsContext();

        [Queryable]
        public IQueryable<Genre> Get()
        {
            return _db.Genres;
        }

        public HttpResponseMessage Get([FromODataUri]int key)
        {
            Genre genre = _db.Genres.SingleOrDefault(b => b.GenreId == key);
            if (genre == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, genre);
        }

        public HttpResponseMessage Put([FromODataUri] int key, Genre genre)
        {
            if (!_db.Genres.Any(g => g.GenreId == key))
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            //overwrite any existing key, as url is more explicit
            genre.GenreId = key;
            _db.Entry(genre).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.NoContent);
        }

        public HttpResponseMessage Post(Genre genre)
        {
            var odataPath = Request.GetODataPath();
            if (odataPath == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                    "ODataPath not present in the request.");
            }

            var entitySetPathSegment
                = odataPath.Segments.FirstOrDefault() as EntitySetPathSegment;

            if (entitySetPathSegment == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                    "ODataPath does not start with entity set path segment");
            }

            Genre addedGenre = _db.Genres.Add(genre);
            _db.SaveChanges();
            var response = Request.CreateResponse(HttpStatusCode.Created, addedGenre);

            response.Headers.Location = new Uri(Url.ODataLink(
                                  entitySetPathSegment,
                                  new KeyValuePathSegment(ODataUriUtils
                                      .ConvertToUriLiteral(addedGenre.GenreId
                                      , ODataVersion.V3))));
            return response;
        }

        public HttpResponseMessage Patch([FromODataUri] int key,
            Delta<Genre> genrePatch)
        {
            Genre genre = _db.Genres
                .SingleOrDefault(p => p.GenreId == key);
            if (genre == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            genrePatch.Patch(genre);
            _db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.NoContent);
        }

        public HttpResponseMessage Delete([FromODataUri] int key)
        {
            Genre genre = _db.Genres.Find(key);
            if (genre == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            _db.Genres.Remove(genre);

            _db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.Accepted);
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}
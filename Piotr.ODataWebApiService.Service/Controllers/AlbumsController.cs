﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using Microsoft.Data.OData;
using Microsoft.Data.OData.Query;
using Piotr.ODataWebApiService.Service.Models;

namespace Piotr.ODataWebApiService.Service.Controllers
{
    [ODataRouting]
    [ODataFormatting]
    public class AlbumsController : ApiController
    {
        private AlbumsContext _db = new AlbumsContext();

        [Queryable]
        public IQueryable<Album> Get()
        {
            return _db.Albums;
        }

        public HttpResponseMessage Get([FromODataUri]int key)
        {
            Album album = _db.Albums.SingleOrDefault(b => b.AlbumId == key);
            if (album == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, album);
        }

        public HttpResponseMessage Put([FromODataUri] int key, Album album)
        {
            if (!_db.Albums.Any(g => g.AlbumId == key))
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            //overwrite any existing key, as url is more explicit
            album.AlbumId = key;
            _db.Entry(album).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.NoContent);
        }

        public HttpResponseMessage Post(Album album)
        {
            var odataPath = Request.GetODataPath();
            if (odataPath == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                    "ODataPath not present in the request.");
            }

            var entitySetPathSegment
                = odataPath.Segments.FirstOrDefault() as EntitySetPathSegment;

            if (entitySetPathSegment == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                    "ODataPath does not start with entity set path segment");
            }

            Album addedAlbum = _db.Albums.Add(album);
            _db.SaveChanges();
            var response = Request.CreateResponse(HttpStatusCode.Created, addedAlbum);

            response.Headers.Location = new Uri(Url.ODataLink(
                                  entitySetPathSegment,
                                  new KeyValuePathSegment(ODataUriUtils
                                      .ConvertToUriLiteral(addedAlbum.AlbumId
                                      , ODataVersion.V3))));
            return response;
        }

        public HttpResponseMessage Patch([FromODataUri] int key,
            Delta<Album> albumPatch)
        {
            Album album = _db.Albums
                .SingleOrDefault(p => p.AlbumId == key);
            if (album == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            albumPatch.Patch(album);
            _db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.NoContent);
        }

        public HttpResponseMessage Delete([FromODataUri] int key)
        {
            Album album = _db.Albums.Find(key);
            if (album == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            _db.Albums.Remove(album);

            _db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.Accepted);
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}
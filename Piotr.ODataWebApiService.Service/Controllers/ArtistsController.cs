﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Routing;
using Microsoft.Data.OData;
using Microsoft.Data.OData.Query;
using Piotr.ODataWebApiService.Service.Models;

namespace Piotr.ODataWebApiService.Service.Controllers
{
    [ODataRouting]
    [ODataFormatting]
    public class ArtistsController : ApiController
    {
        private AlbumsContext _db = new AlbumsContext();

        // GET /Artists
        // GET /Artists?$filter=startswith(Name,'Grid')
        [Queryable]
        public IQueryable<Artist> Get()
        {
            return _db.Artists;
        }

        // GET /Artists(2)
        public HttpResponseMessage Get([FromODataUri]int key)
        {
            Artist artist = _db.Artists.SingleOrDefault(b => b.ArtistId == key);
            if (artist == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, artist);
        }

        public HttpResponseMessage Put([FromODataUri] int key, Artist artist)
        {
            if (!_db.Artists.Any(a => a.ArtistId == key))
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            //overwrite any existing key, as url is more explicit
            artist.ArtistId = key;
            _db.Entry(artist).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.NoContent);
        }

        public HttpResponseMessage Post(Artist artist)
        {
            var odataPath = Request.GetODataPath();
            if (odataPath == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                    "ODataPath not present in the request.");
            }

            var entitySetPathSegment
                = odataPath.Segments.FirstOrDefault() as EntitySetPathSegment;

            if (entitySetPathSegment == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                    "ODataPath does not start with entity set path segment");
            }

            Artist addedArtist = _db.Artists.Add(artist);
            _db.SaveChanges();
            var response = Request.CreateResponse(HttpStatusCode.Created, addedArtist);

            response.Headers.Location = new Uri(Url.ODataLink(
                                  entitySetPathSegment,
                                  new KeyValuePathSegment(ODataUriUtils
                                      .ConvertToUriLiteral(addedArtist.ArtistId
                                      , ODataVersion.V3))));
            return response;
        }

        public HttpResponseMessage Patch([FromODataUri] int key,
            Delta<Artist> artistPatch)
        {
            Artist artist = _db.Artists
                .SingleOrDefault(p => p.ArtistId == key);
            if (artist == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            artistPatch.Patch(artist);
            _db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.NoContent);
        }

        public HttpResponseMessage Delete([FromODataUri] int key)
        {
            Artist artist = _db.Artists.Find(key);
            if (artist == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            _db.Artists.Remove(artist);

            _db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.Accepted);
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}